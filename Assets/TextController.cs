﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextController : MonoBehaviour
{
    private const int HAIRCLIP = 1;
    private const int DRESS = 2;

    public Text text;
    int inventory;
    bool closet_lock;

    private enum States {cell, mirror, sheets_0, lock_0, cell_mirror, sheets_1, lock_1, freedom, corridor_0, stairs__0, floor, closet_door, stairs_0, in_closet};

    private States myState;

	// Use this for initialization
	void Start ()
	{

	    myState = States.cell;


	}
	
	// Update is called once per frame
    void Update()
    {

        print((myState));
        if (myState == States.cell)               {cell();}
        else if (myState == States.sheets_0)      {sheets_0();}
        else if (myState == States.sheets_1)      {sheets_1();}
        else if (myState == States.lock_0)        {lock_0();}
        else if (myState == States.lock_1)        {lock_1();}
        else if (myState == States.mirror)        {mirror();}
        else if (myState == States.cell_mirror)   {cell_mirror();}
        else if (myState == States.corridor_0)    {corridor_0();}
        else if (myState == States.stairs_0)      {stairs_0();}
        else if (myState == States.floor)         {floor();}
        else if (myState == States.closet_door)   {closet_door();}
        else if (myState == States.in_closet)     {in_closet();}
        else if (myState == States.freedom)       {freedom();}
    }

    void cell()
    {
        text.text = "You are in a prison cell, and you want to escape. There are "+
                    "some dirty sheets on the bed, a mirror on the wall, and the door " +
                    "is the locked from the outsidex.\n\n" +
                    "Press S to View the Sheets, M to View the Mirror. L to View the Lock";

        if (Input.GetKeyDown (KeyCode.S))        {myState = States.sheets_0;}
        else if (Input.GetKeyDown(KeyCode.M))    {myState = States.mirror;}
        else if (Input.GetKeyDown(KeyCode.L))    {myState = States.lock_0;}
    }

    void mirror()
    {
        text.text = "The dirty old mirror on the wall seems loose. "+
                    "\n\n" +
                    "\n\n" +
                    "Press T to Take the mirror, or Press R to Return to roaming your cell";

        if (Input.GetKeyDown (KeyCode.T))        {myState = States.cell_mirror;}
        else if (Input.GetKeyDown(KeyCode.R))    {myState = States.cell;}
    }
    void cell_mirror()
    {
        text.text = "You are still in your cell, and you STILL want to escape! There are "+
                    "some dirty sheets on the bed, a mark where the mirror was, " +
                    "and that pesky door is still there, and firmly locked! \n\n" +
                    "Press S to View the Sheets, or Press L to View the Lock";

        if (Input.GetKeyDown (KeyCode.S))        {myState = States.sheets_1;}
        else if (Input.GetKeyDown(KeyCode.L))    {myState = States.lock_1;}
    }
    void sheets_0()
    {
        text.text = "You can't believe you sleep in these things. Surely it's  "+
                    "time somebody changed them. The pleasures of prison life  " +
                    "I guess!.\n\n" +
                    "Press R to Return to roaming your cell";

        if (Input.GetKeyDown (KeyCode.R))        {myState = States.cell;}
    }
    void sheets_1()
    {
        text.text = "Holding a mirror in your hand doesn't make the sheets look "+
                    "any better \n\n" +
                    "\n\n" +
                    "Press R to Return to roaming your cell";

        if (Input.GetKeyDown (KeyCode.R))        {myState = States.mirror;}
    }

    void lock_0()
    {
        text.text = "This is one of those button locks. You have no idea what the "+
                    "combination is. You wish you could somehow see where the dirty " +
                    "fingerprints were, maybe that would help.\n\n" +
                    "Press R to Return to roaming your cell";

        if (Input.GetKeyDown (KeyCode.R))        {myState = States.cell;}
    }

    void lock_1()
    {
        text.text = "You carefully put the mirror through the bars, and turn it round "+
                    "so you can see the lock. You can just make out fingerprints around " +
                    "the buttons. You press the dirty buttons, and hear a click.\n\n" +
                    "Press O to Open the Door, or Press R to Return to roaming your cell";

        if (Input.GetKeyDown(KeyCode.O))        {myState = States.corridor_0;}
        else if (Input.GetKeyDown(KeyCode.R))    {myState = States.cell_mirror;}
    }
    void corridor_0()
    {

            text.text = "You're now in outside the cell and in the corridor  "+
                        "you can feel a breeze and your heart starts to beat with the fear. " +
                        "You look around and asses your current options \n\n" +
                        "Press S to Head towards the Stairs,\n\n" +
                        "Press F to walk the corrdior \n\n" +
                        "or Press C to check the door opposite";


        if (Input.GetKeyDown (KeyCode.S))        {myState = States.stairs_0;}
        else if (Input.GetKeyDown(KeyCode.F))    {myState = States.floor;}
        else if (Input.GetKeyDown(KeyCode.C) & !closet_lock)    {myState = States.closet_door;}
        else if (Input.GetKeyDown(KeyCode.C) & closet_lock)    {myState = States.in_closet;}
    }

    void stairs_0()
    {
        if (inventory <DRESS)
        {
            text.text =
                "You walk up to the stairs and see that there is a guard standing at the top looking out the window, " +
                "you look around but there seems to be no real way up the staircase without alerting him " +
                "\n\n" +
                "Press R to Return to the corridor";

            if (Input.GetKeyDown(KeyCode.R))        {myState = States.corridor_0;}

        } else if (inventory == DRESS)
        {
            text.text =
                "You walk up to the stairs and see that there is a guard standing at the top looking out the window, " +
                "you decide to be brave as you're now waering the uniform. You confidenty walk up the stairs. " +
                "Your heart is beating faster and faster . . .  but you manage to walk by the guard without any issues \n\n" +
                "Press C to Enter the Courtyard";

            if (Input.GetKeyDown(KeyCode.C))        {myState = States.freedom;}

        }
    }

    void closet_door()
    {
        if (inventory ==0)
        {
            text.text = "You see a door, it looks like a way out, " +
                        "you run to the door and try to open it but it won't move " +
                        "then you realise there's a small sign next to the door saying Closet \n\n" +
                        "Press R to Return to the corridor";

            if (Input.GetKeyDown(KeyCode.R))        {myState = States.corridor_0;}
        } else if (inventory == HAIRCLIP)
        {
            text.text = "You see a door, it looks like a way out, " +
                        "you run to the door and try to open it but it won't move. " +
                        "you then realise that there is the hairclip you picked up. \n\n" +
                        "Press P to Pick the lock";

            if (Input.GetKeyDown(KeyCode.P))        {myState = States.in_closet;closet_lock = true;}
        }

    }
    void floor()
    {
        if (inventory == 0)
        {
            text.text = "You continue walking down the corridor, there is a ray of light coming from a small window, " +
                        "it shines onto the floor and makes something shine on the floor, it looks metal, " +
                        "could it be a weapon! wait no its just a lousy hairclip \n\n" +
                        "Press H to Pick up the Hairclip, or Press R to Return to the corridor";

            if (Input.GetKeyDown(KeyCode.H))        {myState = States.corridor_0; inventory= HAIRCLIP;}
            else if (Input.GetKeyDown(KeyCode.R))    {myState = States.corridor_0;}
        } else if (inventory > 0)
        {
            text.text = "You continue walking down the corridor, there is a ray of light coming from a small window, " +
                        "it shines onto the floor and makes something shine on the floor, it looks metal, " +
                        "could it be a weapon! wait no its just a lousy hairclip \n\n" +
                        "Press R to Return to the corridor";

            if (Input.GetKeyDown(KeyCode.R))    {myState = States.corridor_0;}
        }

    }
    void in_closet()
    {
        if (inventory < DRESS)
        {
            text.text = "You picked the lock, turned the handle and enter the dark room. " +
                        "You feel around and turn on the light. In front of you, you see many items. " +
                        "You also see a clean guards uniform, this gets you thinking. \n\n" +
                        "Press D to Dress in the Uniform, or Press R to Return to the corridor";

            if (Input.GetKeyDown(KeyCode.D))        {myState = States.corridor_0; inventory= DRESS;}
            else if (Input.GetKeyDown(KeyCode.R))    {myState = States.corridor_0;}
        } else if (inventory == DRESS)
        {
            text.text = "You open the closet door again. " +
                        "By surprise nothing has changed. " +
                        "\n\n" +
                        "Press R to Return to the corridor";

            if (Input.GetKeyDown(KeyCode.R))    {myState = States.corridor_0;}
        }

    }

    void freedom()
    {
        text.text = "You're FREE!!!! "+
                    "\n\n" +
                    "\n\n" +
                    "Press P to Play Again";

        if (Input.GetKeyDown (KeyCode.P))        {myState = States.cell;inventory = 0;
        }
    }
}
